note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	GAME_TEST_SET

inherit
	EQA_TEST_SET

feature -- Test routines

	test_gutter_game
			-- New test routine
		local game: GAME
		do
			create game.make
			assert ("not_implemented", False)
		end

	roll_many(game: GAME; n: INTEGER; pins: INTEGER)
		local j: INTEGER
		do
			j := n-1
			across
                        0 |..| j as ic
            loop
                        game.roll(pins)
            end
		end
end


